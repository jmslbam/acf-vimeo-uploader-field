<?php

class acf_field_vimeo_uploader extends acf_field
{
	// vars
	var $settings, // will hold info such as dir / path
		$defaults; // will hold default field options

	/*
	*  __construct
	*
	*  Set name / label needed for actions / filters
	*
	*  @since	3.6
	*  @date	23/01/13
	*/

	function __construct()
	{
		// vars
		$this->name = 'vimeo_uploader';
		$this->label = __('Vimeo Uploader');
		$this->category = __("Content",'acf'); // Basic, Content, Choice, etc
		$this->defaults = array(
			'client_id' => '',
			'client_secret' => '',
			'access_token' => '',
			'access_secret' => ''
		);

		// do not delete!
    	parent::__construct();

    	// settings
		$this->settings = array(
			'path' => apply_filters('acf/helpers/get_path', __FILE__),
			'dir' => apply_filters('acf/helpers/get_dir', __FILE__),
			'version' => '1.0.4',
			'nonce_upload' => 'acf_vimeo_uploader_upload',
			'nonce_delete' => 'acf_vimeo_uploader_delete',
			'embed_url' => 'player.vimeo.com/video/'
		);

		add_action( 'post_edit_form_tag',       array($this, 'post_edit_form_tag') );

		// Ajax Upload
		add_action( 'wp_ajax_acf_vimeo_upload', array($this, 'ajax_upload') );
		// Ajax Delete
		add_action( 'wp_ajax_acf_vimeo_delete', array($this, 'ajax_delete') );
	}


	function post_edit_form_tag()
	{
		echo ' enctype="multipart/form-data"';
	}


	function ajax_delete()
	{
   		// vars
		$options = array(
			'nonce' => '',
			'post_id' => 0,
			'key' => ''
		);
		$return = array('status' => 0);

		// load post options
		$options = array_merge($options, $_POST);

		// test options
		foreach ( $options as $key => $option )
		{
			if (! $option ) die('missing option: '. $key);#
		}

		// verify nonce
		if (! wp_verify_nonce($options['nonce'], $this->settings['nonce_delete'] . $options['key']) )
		{
			die('Are you sure you want to do that?');
		}

		#$field = get_field_object($options['key']);
		#if (! $field )
		#{
		#	die('There is not a field available.');
		#}
#echo ' $field[\'name\'] = '. $field['name'] ."<br>\n";
		#delete_post_meta( $options['post_id'], $field['name'] );
		update_field($options['key'], '', $options['post_id']);

		echo 1;
		die;
	}


	function ajax_upload()
	{
   		// vars
		$options = array(
			#'file_id' => '',
			'nonce' => '',
			'post_id' => 0,
			'key' => ''
		);
		$return = array('status' => 0);

		// load post options
		$options = array_merge($options, $_POST);

		// test options
		foreach ( $options as $key => $option )
		{
			if (! $option ) die(0);#die('missing option: '. $key);
		}

		// verify nonce
		if( ! wp_verify_nonce($options['nonce'], $this->settings['nonce_upload']) )
		{
			die(0);#die('Are you sure you want to do that?');
		}

		require_once('vimeo.php');

		// $status = wp_handle_upload($_FILES[$imgid . 'async-upload'], array('test_form' => true, 'action' => 'plupload_action'));
		$field = get_field_object($options['key']);
		if (! $field ) die(0);

		$vimeo = new phpVimeo($field['client_id'], $field['client_secret'], $field['access_token'], $field['access_secret']);

		try
		{
			$video_id = $vimeo->upload($_FILES[$options['key'] . '_file']['tmp_name']);

			if ( $video_id )
			{
				update_field($options['key'], $video_id, $options['post_id']);
				#update_post_meta($options['post_id'], $field['name'], $video_id);

				#$vimeo->call('vimeo.videos.setPrivacy', array('privacy' => 'approved', 'video_id' => $video_id, 'approved_domains' => array(site_url()) ) );
				$vimeo->call(
					'vimeo.videos.setTitle',
					array(
						'title' => apply_filters( 'acf/fields/vimeo/filename', get_the_title($options['post_id']), $options['post_id'] ),
						'video_id' => $video_id
					)
				);
				#$vimeo->call('vimeo.videos.setDescription', array('description' => 'YOUR_DESCRIPTION', 'video_id' => $video_id));

				$return = array(
					'status' => 1,
					'video_id' => $video_id,
					'embed' => $this->vimeo_embed($video_id)
				);
			}
			else
			{
				#wp_die( "Video file did not exist!" );
			}
		}
		catch ( VimeoAPIException $e )
		{
			die( "Encountered an API error -- code {$e->getCode()} - {$e->getMessage()}" );
		}

		// return json
		#echo $video_id;
		echo json_encode($return);
		die;
	}


	/*
	*  create_options()
	*
	*  Create extra options for your field. This is rendered when editing a field.
	*  The value of $field['name'] can be used (like bellow) to save extra data to the $field
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field	- an array holding all the field's data
	*/

	function create_options( $field )
	{
		// defaults?
		/*
		$field = array_merge($this->defaults, $field);
		*/

		// key is needed in the field names to correctly save the data
		$key = $field['name'];


		// Create Field Options HTML
		?>
<tr class="field_option field_option_<?php echo $this->name; ?>">
	<td class="label">
		<label><?php _e("Client ID",'acf'); ?></label>
		<p class="description"><?php _e("(Also known as Consumer Key or API Key)",'acf'); ?></p>
	</td>
	<td>
		<?php

		do_action('acf/create_field', array(
			'type'		=>	'text',
			'name'		=>	'fields['.$key.'][client_id]',
			'value'		=>	$field['client_id'],
			'layout'	=>	'horizontal'
		));

		?>
	</td>
</tr>
<tr class="field_option field_option_<?php echo $this->name; ?>">
	<td class="label">
		<label><?php _e("Client Secret",'acf'); ?></label>
		<p class="description"><?php _e("(Also known as Consumer Secret or API Secret)",'acf'); ?></p>
	</td>
	<td>
		<?php

		do_action('acf/create_field', array(
			'type'		=>	'text',
			'name'		=>	'fields['.$key.'][client_secret]',
			'value'		=>	$field['client_secret'],
			'layout'	=>	'horizontal'
		));

		?>
	</td>
</tr>
<tr class="field_option field_option_<?php echo $this->name; ?>">
	<td class="label">
		<label><?php _e("Access token",'acf'); ?></label>
		<p class="description"><?php _e("Your OAuth Access Token to access your account with this app.",'acf'); ?></p>
	</td>
	<td>
		<?php

		do_action('acf/create_field', array(
			'type'		=>	'text',
			'name'		=>	'fields['.$key.'][access_token]',
			'value'		=>	$field['access_token'],
			'layout'	=>	'horizontal'
		));

		?>
	</td>
</tr>
<tr class="field_option field_option_<?php echo $this->name; ?>">
	<td class="label">
		<label><?php _e("Access token secret",'acf'); ?></label>
		<p class="description"><?php _e("Your OAuth Access Token Secret.",'acf'); ?></p>
	</td>
	<td>
		<?php

		do_action('acf/create_field', array(
			'type'		=>	'text',
			'name'		=>	'fields['.$key.'][access_secret]',
			'value'		=>	$field['access_secret'],
			'layout'	=>	'horizontal'
		));

		?>
	</td>
</tr>
		<?php

	}


	/*
	*  create_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field - an array holding all the field's data
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/

	function create_field( $field )
	{
		$video_id = $field['value'];
		?>
		<input type="hidden" id="<?php esc_attr_e( $field['id'] ); ?>" class="<?php esc_attr_e( $field['class'] ); ?>" name="<?php esc_attr_e( $field['name'] ); ?>" data-key="<?php esc_attr_e( $field['key'] ); ?>" value="<?php esc_attr_e( $field['value'] ); ?>" />

		<div id="<?php esc_attr_e( $field['id'] ); ?>_container" class="<?php esc_attr_e( $field['class'] ); ?>_container">
			<input type="button" id="<?php esc_attr_e( $field['id'] ); ?>_browse" class="button <?php esc_attr_e( $field['class'] ); ?>_browse<?php if ( $video_id ) echo ' '. esc_attr( $field['class'] ) .'_button_hide'; ?>" value="<?php esc_attr_e('Select File'); ?>" />
			<div class="acf_vimeo_filelist"></div>

			<div class="acf_vimeo_upload<?php if ( $video_id ) echo " acf_vimeo_upload_show"; ?>">
				<?php if ( $video_id ) echo $this->vimeo_embed($video_id); ?>
				<div class="acf_vimeo_delete small<?php if ( $video_id ) echo " acf_vimeo_delete_show"; ?>"><a href="#delete" data-key="<?php esc_attr_e( $field['key'] ); ?>" data-nonce="<?php echo wp_create_nonce($this->settings['nonce_delete'] . $field['key']); ?>" title="Delete Video Upload">Delete</a></div>
			</div>
		</div>
		<?php
	}


	/*
	*  input_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
	*  Use this action to add css + javascript to assist your create_field() action.
	*
	*  $info	http://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/

	function input_admin_enqueue_scripts()
	{
		// register acf scripts
		wp_register_script( 'acf-input-vimeo_uploader', $this->settings['dir'] . 'js/input.js', array('acf-input'), $this->settings['version'] );
		$args = array(
			'url'                  => admin_url( 'admin-ajax.php' ),
			'runtimes'             => 'html5,silverlight,flash,html4',
			'browse_button'        => '_browse',
			'container'            => '_container',
			'drop_element'         => '_drop',
			'file_data_name'       => '_file',
			'multiple_queues'      => true,
			'max_file_size'        => wp_max_upload_size() . 'b',
			'flash_swf_url'        => includes_url('js/plupload/plupload.flash.swf'),
			'silverlight_xap_url'  => includes_url('js/plupload/plupload.silverlight.xap'),
			'filters'              => array(array('title' => __('Allowed Files'), 'extensions' => '*')),
			'multipart'            => true,
			'urlstream_upload'     => true,
			'multi_selection'      => false,
			// additional post data to send to our ajax hook
			'multipart_params'     => array(
				#'file_id'              => '', // will be added per uploader
				'action'               => 'acf_vimeo_upload',
				'nonce'                => wp_create_nonce( $this->settings['nonce_upload'] ),
				'post_id'              => ( isset($GLOBALS['post']) && is_object($GLOBALS['post']) ? $GLOBALS['post']->ID : 0),
				'key'                  => '',
				'spinner'              => admin_url( 'images/loading.gif' )
			)
		);
		wp_localize_script( 'acf-input-vimeo_uploader', 'acf_vimeo', $args );

		wp_enqueue_script(array(
			'jQuery',
			'plupload',
			'plupload-all',
			'acf-input-vimeo_uploader'
		));

		// styles
		wp_register_style( 'acf-input-vimeo_uploader', $this->settings['dir'] . 'css/input.css', array('acf-input'), $this->settings['version'] );

		wp_enqueue_style(array(
			'acf-input-vimeo_uploader'
		));
	}


	/*
	*  format_value_for_api()
	*
	*  This filter is appied to the $value after it is loaded from the db and before it is passed back to the api functions such as the_field
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value	- the value which was loaded from the database
	*  @param	$post_id - the $post_id from which the value was loaded
	*  @param	$field	- the field array holding all the field options
	*
	*  @return	$value	- the modified value
	*/

	function format_value_for_api($value, $post_id, $field)
	{
		if ( $value )
		{
			$value = $this->vimeo_embed($field['value']);
		}
		return $value;
	}

	function embed_video( $url )
	{
		$output = '<div class="acf_vimeo_video"><iframe src="' . esc_attr($url) . '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>';
		return $output;
	}

	function http()
	{
		return (is_ssl() ? 'https' : 'http') . '://';
	}

	function vimeo_url( $video_id )
	{
		return $this->http() . $this->settings['embed_url'] . $video_id;
	}

	function vimeo_embed( $video_id )
	{
		return $this->embed_video( $this->vimeo_url($video_id) );
	}
}


// create field
new acf_field_vimeo_uploader();
