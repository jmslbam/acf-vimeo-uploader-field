# ACF { Vimeo Uploader

Consider this plugin somewhat BETA. It is free, and it is in use in production, but there are still things being figured out for different use cases as needed. Give it a try, and let me know how it might be modified to suit your needs and I will make an effort get those updates included.

* works in ACF version 4
* does NOT currently work in ACF version 3 (I was lazy and I didn't need it)
* requires a VIMEO PRO account
* requires that your VIMEO PRO account has an API app created
* requires that your API app has requested and been approved for upload, this isn't very hard, but can take 2-3 days

### Overview

This was built using [Elliot Condon's Field Type Template](https://github.com/elliotcondon/acf-field-type-template) for [Advance Custom Fields](http://www.advancedcustomfields.com/). So it requires that you install the free ACF plugin first. Once both plugins are installed, you can use ACF to add custom meta fields to posts, pages, and users. This plugin will added a video upload field as an option.

This plugin uses plupload which is included with Wordpress by default to manage the upload and create a progress bar during upload. Once uploaded, it uses your API info to send the video to your Vimeo PRO account where it can be transcoded and hosted. The video is not stored on your server beyond the initial upload to a temp folder (which is unavoidable).

### Compatibility

This add-on will work with:

* version 4 and up
* NOT version 3 and below
* Untested in version 5 currently


### Installation

This add-on can be treated as both a WP plugin and a theme include.

**Install as Plugin**

1. Copy the 'acf-vimeo-uploader-field' folder into your plugins folder
2. Activate the plugin via the Plugins admin page

**Include within theme**

1.	Copy the 'acf-vimeo-uploader-field' folder into your theme folder (can use sub folders). You can place the folder anywhere inside the 'wp-content' directory
2.	Edit your functions.php file and add the code below (Make sure the path is correct to include the acf-vimeo-uploader.php file)

```php
include_once('acf-vimeo-uploader/acf-vimeo-uploader.php');
```
