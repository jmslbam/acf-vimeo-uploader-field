=== Advanced Custom Fields: Vimeo Uploader Field ===
Contributors: jcow
Tags:
Requires at least: 3.4
Tested up to: 3.9
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Allows you to upload to a single vimeo account that has upload API access.

== Description ==

Consider this plugin somewhat BETA. It is free, and it is in use in production, but there are still things being figured out for different use cases as needed. Give it a try, and let me know how it might be modified to suit your needs and I will make an effort get those updates included.

* works in ACF version 4
* does NOT currently work in ACF version 3 (I was lazy and I didn't need it)
* requires a VIMEO PRO account
* requires that your VIMEO PRO account has an API app created
* requires that your API app has requested and been approved for upload, this isn't very hard, but can take 2-3 days

= Compatibility =

This add-on will work with ACF versions:

* version 4 and up
* NOT version 3 and below
* Untested in version 5 currently

== Installation ==

This add-on can be treated as both a WP plugin and a theme include.

= Plugin =
1. Copy the 'acf-vimeo_uploader' folder into your plugins folder
2. Activate the plugin via the Plugins admin page

= Include =
1.	Copy the 'acf-vimeo_uploader' folder into your theme folder (can use sub folders). You can place the folder anywhere inside the 'wp-content' directory
2.	Edit your functions.php file and add the code below (Make sure the path is correct to include the acf-vimeo_uploader.php file)

`
add_action('acf/register_fields', 'my_register_fields');

function my_register_fields()
{
	include_once('acf-vimeo_uploader/acf-vimeo_uploader.php');
}
`

== Changelog ==

= 0.0.1 =
* Initial Release.
