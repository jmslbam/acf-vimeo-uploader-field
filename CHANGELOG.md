# Changelog


## 1.0.6 - 2014-06-02

- Fixed some delete issues

## 1.0.5 - 2014-05-22

- Deleted the old v3 file
- Some cleanup

## 1.0.3 - 2014-04-22

- Fixed delete problems
- Fixed post_id problem when no global $post object is available
- Fixed uploads to wrong post when form is updating user or taxonomy, now it is using the same post_id as the form

## 1.0.0 - 2013

- Initial release
